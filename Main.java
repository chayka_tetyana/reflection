import java.lang.reflect.Field;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the class name (Animal or Cat): ");
        String className = scanner.nextLine();

        try {
            Class<?> selectedClass = Class.forName(className);

            System.out.println("Fields of " + className + ":");
            Field[] fields = selectedClass.getDeclaredFields();
            for (Field field : fields) {
                String modifiers = "";
                if (field.getModifiers() == Field.PUBLIC) {
                    modifiers = "public";
                } else if (field.getModifiers() == Field.PUBLIC) {
                    modifiers = "protected";
                } else if (field.getModifiers() == Field.PUBLIC) {
                    modifiers = "private";
                }
                System.out.println(modifiers + " " + field.getType().getName() + " " + field.getName());
            }
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found: " + className);
        }
    }
}
